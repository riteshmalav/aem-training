package com.aem.cloud.training.core.models;

import com.aem.cloud.training.core.services.PathConfig;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;


@Model(adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class TitleChangeModel {

    @OSGiService
    private PathConfig pathConfig;

    private String pathvalue;

    private final static Logger logger = LoggerFactory.getLogger(TitleChangeModel.class);

    @PostConstruct
    protected void init() {
        pathvalue = pathConfig.getServletpath();
        logger.info("pathvalue PostConstruct methed inside ::::::::::{}", pathvalue);
    }

    public String getPathvalue() {
        logger.info("pathvalue  ::::::::::{}", pathvalue);
        return pathvalue;
    }
}
