package com.aem.cloud.training.core.services.impl;

import com.aem.cloud.training.core.services.Activities;
import com.aem.cloud.training.core.services.PathConfig;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(service = PathConfig.class, immediate = true)
@Designate(ocd = PathConfigImpl.ServletPathConfig.class)
public class PathConfigImpl implements PathConfig {

    private static final Logger logger = LoggerFactory.getLogger(PathConfigImpl.class);


    @ObjectClassDefinition(name = "Servlet Path ", description = "ADD NEW PATH AS REQUIREDMENT ")
     @interface ServletPathConfig {
        @AttributeDefinition(
                name = "Servlet Path", description = "Add new Path", type = AttributeType.STRING
        )
        String servletpath() default "/bin/demovalue";
    }

    private String path;

    @Activate
    protected void activate(ServletPathConfig ServletPathConfig) {
       this.path = ServletPathConfig.servletpath();
       logger.info("path value is ::::::::::{}",path);
    }

    @Override
    public String getServletpath() {
        return path;
    }

}
