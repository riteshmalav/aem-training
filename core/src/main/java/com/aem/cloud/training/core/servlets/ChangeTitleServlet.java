package com.aem.cloud.training.core.servlets;

import com.aem.cloud.training.core.services.TitleUpdateService;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.json.JSONException;
import org.json.JSONObject;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;
import java.io.PrintWriter;

@Component(service = Servlet.class,
        property = {
                "sling.servlet.paths=" + "/bin/titleupdatecomp",
                "sling.servlet.methods=" + HttpConstants.METHOD_POST
        }
)
public class ChangeTitleServlet extends SlingAllMethodsServlet {

    private final static Logger logger = LoggerFactory.getLogger(ChangeTitleServlet.class);
    @Reference
    private TitleUpdateService titleUpdateService;

    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        try {
            logger.debug("hello::::::::::::::::::::::");
            String title = request.getParameter("titlevalue");
            logger.info("title ::::{}", title);
            boolean status = titleUpdateService.updatedata(title);
            if (status) {
                JSONObject resJson = new JSONObject();
                resJson.put("success", "true");
                resJson.put("message", "Title updated");
                resJson.put("status", "200");
                out.println(resJson);
            } else {
                out.println("title is not update");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}

